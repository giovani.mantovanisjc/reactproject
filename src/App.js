import React from 'react';

import './styles/global';

import Sidebar from './components/Sidebar';

const App = () => <Sidebar />;

export default App;
